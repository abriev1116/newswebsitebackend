# Use an official Python runtime as a parent image
FROM python:3.9.9

# Set the working directory in the container
WORKDIR /usr/src/app


ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Upgrade pip
RUN pip install --upgrade pip

# Copy the project's requirements file into the container
COPY requirements.txt /usr/src/app/

# Install project dependencies
RUN pip install -r requirements.txt

# Copy the project files into the container
COPY . /usr/src/app/

