# Paid news service API


## Necessary technologies

Docker20.10.25 version or above. Installation guide [here](https://docs.docker.com/engine/install/).

Docker-compose 1.29.2 version or above. Installation guide [here](https://docs.docker.com/compose/install/)

Python 3.8 version or above. Installation guide [here](https://www.python.org/downloads/)


# Launch the project
1. Create an environment named .env in the main folder of the project
2. Edit the .env.dev file as in the .env.dev.example file

3. Start up Docker containers and build any images that are defined in the Dockerfiles 
```bash
docker-compose up --build
```
4  After the containers are created, create a superuser for django admin
```
docker-compose run web python manage.py createsuperuser
```
6 Press ```ctrl+c``` to stop containers

7 To delete containers and their associated images
```
docker-compose down
```
To delete container, image, volume and network
```
docker-comose down -v
```
Warning: the database will be deleted after this command
