from rest_framework import serializers

from core.models import News
from ..serializers import HashTagSerializers


class HistoryOfNewsReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ("id", "title", "created_at")


