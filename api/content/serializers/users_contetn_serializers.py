from rest_framework import serializers
from core.models import News, HashTags


class HashTagSerializers(serializers.ModelSerializer):
    class Meta:
        model = HashTags
        exclude = ("creator", )


class NewsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ("id", "title", "to_see", "created_at")


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        exclude = ("creator", "viewers",)

    def to_representation(self, instance):
        """convert hashtags from [id, id, id ] to [{"id":int id hashtag, "hashtag":str hashtag}]"""
        data = super(NewsSerializer, self).to_representation(instance)
        data['hashtags'] = HashTagSerializers(instance=instance.hashtags.all(), many=True).data
        data['views'] = instance.views_count
        return data


