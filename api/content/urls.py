from django.urls import path
from rest_framework.routers import SimpleRouter

from .views import users_content_views, history_views
router = SimpleRouter()
router.register("hashtag", users_content_views.HashtagViewset, basename="users_hashtags")
router.register("news", users_content_views.NewsViewset, basename="users_news")

urlpatterns = [
        path("history/of/news/read/", history_views.HistoryOfNewsReadView.as_view(), name="history")
] + router.urls
