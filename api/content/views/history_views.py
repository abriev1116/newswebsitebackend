from rest_framework.generics import ListAPIView

from api.permissions import UserPermission
from ..serializers import HistoryOfNewsReadSerializer


class HistoryOfNewsReadView(ListAPIView):
    serializer_class = HistoryOfNewsReadSerializer
    permission_classes = [UserPermission]

    def get_queryset(self):
        user = self.request.user
        queryset = user.views.all()
        return queryset
