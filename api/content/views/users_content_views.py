from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from api.constants import *
from api.permissions import UserPermission
from core.models import HashTags, News
from ..serializers import HashTagSerializers, NewsSerializer, NewsListSerializer


class HashtagViewset(ModelViewSet):
    serializer_class = HashTagSerializers
    permission_classes = [UserPermission]
    queryset = HashTags.objects.all()

    def get_queryset(self):
        """Get the user's own hashtags"""
        return super().get_queryset().filter(creator=self.request.user)

    def create(self, request, *args):
        """The tag creator is obtained from request.user"""
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(creator=self.request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        tag = self.get_object()
        if tag.tag_news.all().exists():
            return Response({"sstatus": ERROR, "message": "there are news attached to this hashtag"},
                            status.HTTP_400_BAD_REQUEST)
        tag.delete()
        return Response({"sstatus": SUCCESS, "message": "deleted successfully"}, status.HTTP_200_OK)


class NewsViewset(ModelViewSet):
    serializer_class = NewsSerializer
    queryset = News.objects.all()
    permission_classes = [UserPermission]

    def get_serializer_class(self):
        """
           In order not to increase the size of the response with all the information related to the news object
           in the news list, only the id, title and to_see fields are returned.
        """
        if self.action == "list":
            return NewsListSerializer
        return self.serializer_class

    def get_queryset(self):
        """Get the user's own news"""
        return super().get_queryset().filter(creator=self.request.user)

    def create(self, request, *args):
        """The tag creator is obtained from request.user"""
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(creator=self.request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
