from rest_framework import serializers

from core.models import News, HashTags


class AllNewsListSerializer(serializers.Serializer):
    """
        Due to the fact that only GET requests come to the news list api and the  requests count
        compared to other apis is large, a simple Serializer was used instead of Modelserializer.
        In this situation, the simple serializer is faster than the model serializer
    """
    id = serializers.IntegerField()
    title = serializers.CharField()
    created_at = serializers.DateTimeField()
    views = serializers.IntegerField()

    class Meta:
        fields = "__all__"


class HashTagSerializers(serializers.ModelSerializer):
    class Meta:
        model = HashTags
        exclude = ("creator",)


class NewsDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        exclude = ("viewers", "to_see")

    def to_representation(self, instance):
        """convert hashtags from [id, id, id ] to [{"id":int id hashtag, "hashtag":str hashtag}]"""
        data = super(NewsDetailSerializer, self).to_representation(instance)
        data['hashtags'] = HashTagSerializers(instance=instance.hashtags.all(), many=True).data
        data['views'] = instance.views_count
        data['creator'] = f"{instance.creator.first_name}  {instance.creator.last_name}"
        return data

