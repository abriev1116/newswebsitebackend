from django.urls import path

from .views import news_views

urlpatterns = [
    path("list/", news_views.NewsListView.as_view(), name="news_list"),
    path("detail/<int:pk>/", news_views.NewsDetailView.as_view(), name="news_detail")
]
