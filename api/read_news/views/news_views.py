from django.db.models import Count
from rest_framework import status
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.response import Response

from api.permissions import UserPermission
from core.models import News
from core.pagination import CustomPaginationWithPageSize
from ..serializers import AllNewsListSerializer, NewsDetailSerializer


class NewsListView(ListAPIView):
    queryset = News.objects.filter(to_see=True).annotate(
        views=Count("viewers")).values("id", "title", "created_at", "views")
    serializer_class = AllNewsListSerializer
    pagination_class = CustomPaginationWithPageSize
    """
     The news list is open to users, no authorization is needed to get this list.
     If the user has done authorization, no money will be taken from his account.
    """


class NewsDetailView(GenericAPIView):
    serializer_class = NewsDetailSerializer
    queryset = News.objects.filter(to_see=True)
    permission_classes = [UserPermission]

    def get(self, request, *args, **kwargs):
        news = self.get_object()
        creator = news.creator
        reader = self.request.user

        # Check if the news belongs to the reader's creator
        if creator == reader:
            response_data = self.serializer_class(instance=news).data
        else:
            # Check if the user has already viewed the news
            if news.viewers.filter(id=reader.id).exists():
                response_data = self.serializer_class(instance=news).data

            else:
                fee = creator.fee_for_reading_news
                half_fee = creator.half_of_the_fee

                # Check if the news is free to read
                if fee == 0:
                    news.viewers.add(reader)
                    news.save()
                    response_data = self.serializer_class(instance=news).data

                # Check if the reader has enough balance to read the news
                elif reader.balance >= fee:
                    # Deduct the fee from the reader's balance
                    reader.balance -= fee
                    reader.save()

                    # Add half of the fee to the creator's balance
                    creator.balance += half_fee
                    creator.save()

                    # Add the reader to the list of news viewers
                    news.viewers.add(reader)
                    news.save()

                    response_data = self.serializer_class(instance=news).data
                else:
                    response_data = {"status": "ERROR", "message": "You do not have enough funds to read this news"}
                    return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

        return Response(response_data)
