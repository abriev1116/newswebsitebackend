from rest_framework import serializers
from core.models import CustomUser


class CreateAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ("first_name", "last_name", "fee_for_reading_news",  "username", "password")


class LogInSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ("username", "password")

