from rest_framework import serializers
from core.models import CustomUser


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ("id", "first_name", "last_name", "balance", "fee_for_reading_news")
        extra_kwargs = {
            "balance": {'read_only': True},
        }

