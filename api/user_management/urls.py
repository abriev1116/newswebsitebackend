from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from .views import authentication_views, profile_views

urlpatterns = [
    # authentication urls
    path("signup/", authentication_views.CreateAccountView.as_view(), name="sign_up"),
    path("signin/", TokenObtainPairView.as_view(), name="login"),
    path("refresh/", TokenRefreshView.as_view(), name="refresh"),

    # profile urls
    path("profile/", profile_views.ProfileView.as_view(), name="profile")
]

