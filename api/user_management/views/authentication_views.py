from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from api.constants import *
from core.models import CustomUser
from ..serializers import CreateAccountSerializer


class CreateAccountView(CreateAPIView):
    serializer_class = CreateAccountSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Create a new user
        CustomUser.objects.create_user(**serializer.validated_data)

        return Response({"status": SUCCESS, "message": "You have successfully registered."},
                        status.HTTP_201_CREATED)
