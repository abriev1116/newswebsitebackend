from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from ..serializers import ProfileSerializer
from api.permissions import UserPermission


class ProfileView(GenericAPIView):
    serializer_class = ProfileSerializer
    permission_classes = [UserPermission]
    pagination_class = None

    def get(self, request):
        """
            To get profile information, it is enough to send a request by putting Bearer access token in the url header
        """
        user = self.request.user
        response_data = self.serializer_class(instance=user).data
        return Response(response_data)

    def patch(self, request):
        user = self.request.user
        first_name = self.request.data.get("first_name")
        last_name = self.request.data.get("last_name")
        fee_for_reading_news = self.request.data.get("fee_for_reading_news")
        if first_name:
            user.first_name = first_name
        if last_name:
            user.last_name = last_name
        if fee_for_reading_news:
            user.fee_for_reading_news = fee_for_reading_news
        user.save()
        response_data = self.serializer_class(instance=user).data

        return Response(response_data)
