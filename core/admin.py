from django.contrib import admin
from .models import user, content


@admin.register(user.CustomUser)
class CustomUserModelAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "balance")
    list_display_links = ("id", "username")


@admin.register(content.HashTags)
class HashtagsAdmin(admin.ModelAdmin):
    list_display = ("id", "tag", "creator")
    list_display_links = ("id", "tag", "creator")


@admin.register(content.News)
class NewAdmin(admin.ModelAdmin):
    list_display = ("id", "creator", "to_see")
    list_display_links = ("id", "creator")
