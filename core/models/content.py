from django.db import models
from django.contrib.auth import get_user_model


class HashTags(models.Model):
    tag = models.CharField(max_length=50)
    creator = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="hashtag")


class News(models.Model):
    creator = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="news")
    title = models.CharField(max_length=255)
    description = models.TextField()
    hashtags = models.ManyToManyField(HashTags, blank=True, null=True, related_name="tag_news")
    to_see = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    viewers = models.ManyToManyField(get_user_model(), related_name="views", blank=True, null=True)


    @property
    def views_count(self) -> object:
        """this function is to count how many people have seen the news"""
        return self.viewers.all().count()

