from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    balance = models.FloatField(default=0, verbose_name="Account balance")
    fee_for_reading_news = models.IntegerField(default=0)

    def __str__(self):
        return self.username
    
    @property
    def half_of_the_fee(self):
        if self.fee_for_reading_news == 0:
            return 0
        else:
            return self.fee_for_reading_news // 2
