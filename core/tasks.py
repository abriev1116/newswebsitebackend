import random

from celery import shared_task

from core.models import CustomUser


@shared_task(bind=True)
def increase_random_user_balance(self):
    # Get all users from the CustomUser model
    all_users = CustomUser.objects.filter(is_staff=False)

    if all_users.exists():
        # Select a random user
        random_user = random.choice(all_users)

        # Generate a random balance
        random_balance = random.randint(1, 100)

        # Update the selected user's balance
        random_user.balance += random_balance
        random_user.save()

        return "random user's balance was replenished to random amount"
    return "no users found in the CustomUser model."
