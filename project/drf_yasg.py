from django.contrib import admin
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework_swagger.views import get_swagger_view

API_TITLE = 'News Website Backend Docs'
API_DESCRIPTION = """Restfull api documentation for news website. To use apis that require authorization,
                     enter the data in the signin api, copy the access token and press the Authorize button and
                     put the token "Bearer token" in this form
                  """
yasg_schema_view = get_schema_view(
    openapi.Info(
        title=API_TITLE,
        default_version='v1',
        description=API_DESCRIPTION,
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="jurabekabriev@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
)

schema_view = get_swagger_view(title=API_TITLE)

admin.site.site_header = "News Website Backend Admin"
admin.site.site_title = "News Website Backend Admin"
admin.site.index_title = "News Website Backend Admin"
