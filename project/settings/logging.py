import logging.handlers

FORMATTERS = {
    "verbose": {
        "format": "{levelname} {asctime:s} {threadName} {thread:d} {module} {filename} {lineno:d} {name} {funcName} {process:d} {message}",
        "style": "{",
    },
    "simple": {
        "format": "{levelname} {asctime:s} {module} {filename} {lineno:d} {funcName} {message}",
        "style": "{",
    },
}

HANDLERS = {
    "console_handler": {
        "class": "logging.StreamHandler",
        "formatter": "simple",
    },
    "handler_detailed": {
        "class": "logging.handlers.RotatingFileHandler",
        "filename": "backend_data_detailed.log",
        "mode": "a",
        "formatter": "verbose",
        "backupCount": 5,
        "maxBytes": 1024 * 1024 * 2,  # 2 MB
    },
}

LOGGERS = {
    "django": {
        "handlers": ["console_handler"],
        "level": "INFO",
        "propagate": False,
    },
    "django.request": {
        "handlers": ["handler_detailed"],
        "level": "WARNING",
        "propagate": False,
    },
}

LOGGING_CONF = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": FORMATTERS,
    "handlers": HANDLERS,
    "loggers": LOGGERS,
}
